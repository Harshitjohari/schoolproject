from tkinter import *
from PIL import ImageTk
from PIL import Image,ImageTk
from tkinter import ttk
from tkinter import messagebox
import tkinter as tk
from tkinter import Tk


class Login_System:      
    def __init__(self,root):
        self.root=root
        self.root.title("SCHOOL A.I")
        self.root.geometry("")
        #self.root.overrideredirect(True)
        self.root.wm_attributes('-type', 'splash')
        self.bg_icon=ImageTk.PhotoImage(file="images/a2.jpg")
        self.user_icon=PhotoImage(file="images/u.png")
        self.pass_icon=PhotoImage(file="images/p.png")
        self.logo_icon=PhotoImage(file="images/logo.png")
        
        self.uname=StringVar()
        self.pass_=StringVar()
        
        bg_lbl=Label(self.root,image=self.bg_icon).pack()
               
        title=Label(self.root,text="School A.I ADMIN LOGIN",font=("times new roman",40,"bold"),bg="light blue",fg="grey",bd=10,relief=GROOVE)
        title.place(x=620,y=250)

        Login_Frame=Frame(self.root,bg="light blue")
        Login_Frame.place(x=700,y=350)
        logolbl=Label(Login_Frame,image=self.logo_icon,bd=0).grid(row=0,columnspan=2 ,pady=20)

        lbluser=Label(Login_Frame,text="Username",image=self.user_icon,compound=LEFT,font=("times new roman",20,"bold"),bg="light blue").grid(row=1,column=0,padx=20,pady=10)
        txtuser=Entry(Login_Frame,bd=5,textvariable=self.uname,relief=GROOVE,font=("",15)).grid(row=1,column=1,padx=20)

        lblpass=Label(Login_Frame,text="Password",image=self.pass_icon,compound=LEFT,font=("times new roman",20,"bold"),bg="light blue").grid(row=2,column=0,padx=20,pady=10)
        txtpass=Entry(Login_Frame,bd=5,show="*",relief=GROOVE,textvariable=self.pass_,font=("",15)).grid(row=2,column=1,padx=20)
 
        btn_log=Button(Login_Frame,text="Login",width=15,command=self.login,font=("times new roman",14,"bold"),bg="light blue",fg="black").grid(row=3,columnspan=2,pady=10)
        btn_quit=Button(Login_Frame,text="Quit",width=15,command=root.destroy,font=("times new roman",14,"bold"),bg="light blue",fg="black").grid(row=4,columnspan=2,pady=10)
       
        
                     

    def login(self):
        if self.uname.get()=="" and self.pass_.get()=="":
            messagebox.showerror("Error","All fields are required!!")
        elif self.uname.get()=="admin" and self.pass_.get()=="123":
            Login_System.login_sucess()
            
            
        else:
            messagebox.showerror("Error","Invalid username or password")      

    def logout(self):
        root.destroy()
                
   
    def login_sucess():
        win = tk.Tk()
        
        win.title("SCHOOL A.I")
        def live_feed():      
            win = tk.Tk()
            win.title("SCHOOL A.I")
            win.geometry("{0}x{1}+0+0".format(win.winfo_screenwidth(), win.winfo_screenheight()))
            win.wm_attributes('-type', 'splash')
            win.configure(background='light blue')
            title=Label(win,text="Live Feed",font=("times new roman",25,"bold","underline"),bg="light blue",fg="black",bd=5,relief=GROOVE)
            title.place(x=800,y=5)

            #combo box 1
            cls=["...","I","II","III","IV","V","VI","VII","VII","IX","X","XI","XII"]
            label5=Label(win,text="Select Class:",font=("times new roman",15,"bold"),bg="light blue",bd=3)
            label5.place(x=670,y=60)
            cb=ttk.Combobox(win,values=cls,width=5)
            cb.place(x=790,y=60)
            cb.current(0)

            #combo box 2
            sectn=["...","A","B","C","D"]
            label6=Label(win,text="Select Section:",font=("times new roman",15,"bold"),bg="light blue",bd=3)
            label6.place(x=870,y=60)
            cb=ttk.Combobox(win,values=sectn,width=5)
            cb.place(x=1000,y=60)
            cb.current(0)

            #submit
            Button7=Button(win, text="Submit",font=("times new roman",12,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
            Button7.place(x=1090,y=60)

            #back
            Button8=Button(win,text="Back", command=win.destroy,font=("times new roman",20,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
            Button8.place(x=1500,y=5)

            labelframe2=LabelFrame(win,height=800,width=1600,bd=2,relief=SUNKEN)  
            labelframe2.place(x=160,y=100)

        win.geometry("{0}x{1}+0+0".format(win.winfo_screenwidth(), win.winfo_screenheight()))
        #win.resizable(0,0)
        win.wm_attributes('-type', 'splash')
        #win.overrideredirect(True)
        win.configure(background='light blue')
        title=Label(win,text="DATA CREATER",font=("times new roman",25,"bold","underline"),bg="light blue",fg="black",bd=5,relief=GROOVE)
        title.place(x=800,y=5)
        #roll. no
        label1 =Label(win, text='Enter Roll no. : ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
        label1.place(x=710,y=60)
        entry1 =Entry(win,width=30,bd=2)
        entry1.place(x=850,y=60)
        Button1=Button(win,text='Submit',font=("times new roman",14,"bold"),bg="light blue",activeforeground="light blue",activebackground="grey")
        Button1.place(x=1100,y=60)
        #name
        label2 =Label(win, text='Enter Name : ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
        label2.place(x=710,y=100)
        entry2 =Entry(win,width=30,bd=2)
        entry2.place(x=850,y=100)
        Button2=Button(win,text='Submit',font=("times new roman",14,"bold"),bg="light blue",activeforeground="light blue",activebackground="grey")
        Button2.place(x=1100,y=100)
        #Gender
        gender=IntVar()
        label3 = Label(win, text='Select Gender: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
        label3.place(x=710,y=140)
        rad1 = Radiobutton(win,text='Male',variable=gender ,value=1,font=("times new roman",14,"bold"),bg="light blue",activebackground="light blue")
        rad2 = Radiobutton(win,text='Female',variable=gender ,value=2,font=("times new roman",14,"bold"),bg="light blue",activebackground="light blue")
        rad1.place(x=850,y=140)
        rad2.place(x=950,y=140)
        #role
        role=IntVar()
        label4 = Label(win, text='Select Role: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
        label4.place(x=710,y=180)
        rad3 = Radiobutton(win,text='Student',variable=role,value=3,font=("times new roman",14,"bold"),bg="light blue",activebackground="light blue")
        rad4 = Radiobutton(win,text='Teacher',variable=role ,value=4,font=("times new roman",14,"bold"),bg="light blue",activebackground="light blue")
        rad3.place(x=850,y=180)
        rad4.place(x=950,y=180)
        #take images
        Button4=Button(win,text='Take Images',font=("times new roman",25,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
        Button4.place(x=850,y=300)
        labelframe1=LabelFrame(win,height=500,width=500,bd=2,relief=SUNKEN)  
        labelframe1.place(x=710,y=360)
        #train images
        Button5=Button(win,text='Train Images',font=("times new roman",25,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
        Button5.place(x=850,y=875)

        #live feed
        Button6=Button(win,text='View Live Feed',command=live_feed,font=("times new roman",30,"bold"),bg="light blue",height=3,width=20,bd=6,relief=GROOVE,activebackground="grey",activeforeground="light blue")
        Button6.place(x=1300,y=5)

        #logout button
        Button6=Button(win, text="LogOut", command=win.destroy,font=("times new roman",25,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
        Button6.place(x=1400,y=875)

        win.mainloop()
                    
    
root=Tk()
obj=Login_System(root)
root.mainloop()

