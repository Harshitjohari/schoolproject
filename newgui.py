from tkinter import *
from PIL import ImageTk
from PIL import Image,ImageTk
from tkinter import ttk
from tkinter import messagebox
import tkinter as tk
from tkinter import Tk
import os
import cv2
import numpy as np 
from PIL import Image
import sqlite3
import datetime
from array import *
import numpy as np




class Login_System:      
    def __init__(self,root):
        self.root=root
        self.root.title("SCHOOL A.I")
        self.root.geometry("")
        #self.root.overrideredirect(True)
       # self.root.wm_attributes('-type', 'splash')
        self.bg_icon=ImageTk.PhotoImage(file="images/a2.jpg")
        self.user_icon=PhotoImage(file="images/u.png")
        self.pass_icon=PhotoImage(file="images/p.png")
        self.logo_icon=PhotoImage(file="images/logo.png")
        
        self.uname=StringVar()
        self.pass_=StringVar()
        
        bg_lbl=Label(self.root,image=self.bg_icon).pack()
               
        title=Label(self.root,text="School A.I ADMIN LOGIN",font=("times new roman",40,"bold"),bg="light blue",fg="grey",bd=10,relief=GROOVE)
        title.place(x=620,y=250)

        Login_Frame=Frame(self.root,bg="light blue")
        Login_Frame.place(x=700,y=350)
        logolbl=Label(Login_Frame,image=self.logo_icon,bd=0).grid(row=0,columnspan=2 ,pady=20)
        
        lbluser=Label(Login_Frame,text="Username",image=self.user_icon,compound=LEFT,font=("times new roman",20,"bold"),bg="light blue").grid(row=1,column=0,padx=20,pady=10)
        txtuser=Entry(Login_Frame,bd=5,textvariable=self.uname,relief=GROOVE,font=("",15)).grid(row=1,column=1,padx=20)
        
        lblpass=Label(Login_Frame,text="Password",image=self.pass_icon,compound=LEFT,font=("times new roman",20,"bold"),bg="light blue").grid(row=2,column=0,padx=20,pady=10)
        txtpass=Entry(Login_Frame,bd=5,show="*",relief=GROOVE,textvariable=self.pass_,font=("",15)).grid(row=2,column=1,padx=20)
 
        btn_log=Button(Login_Frame,text="Login",width=15,command=self.login,font=("times new roman",14,"bold"),bg="light blue",fg="black").grid(row=3,columnspan=2,pady=10)
        btn_quit=Button(Login_Frame,text="Quit",width=15,command=root.destroy,font=("times new roman",14,"bold"),bg="light blue",fg="black").grid(row=4,columnspan=2,pady=10)
       
        
                     

    def login(self):
        if self.uname.get()=="" and self.pass_.get()=="":
            messagebox.showerror("Error","All fields are required!!")
        elif self.uname.get()=="a" and self.pass_.get()=="a":
            self.uname.set("")
            self.pass_.set("")
            Login_System.login_sucess()
                        
        else:
            messagebox.showerror("Error","Invalid username or password")      

    def logout(self):
        root.destroy()
                
   
    def login_sucess():
        win = tk.Tk()
        win.title("SCHOOL A.I")
        def live_feed():      
            def detector():
                recognizer=cv2.face.LBPHFaceRecognizer_create()
                recognizer.read("/home/harshit/Desktop/schoolproject/trainner/trainingData.yml")
                faceDetect=cv2.CascadeClassifier('/home/harshit/Desktop/schoolproject/haarcascade_frontalface_default.xml')
                def getProfile(rollno):
                    conn=sqlite3.connect('school.db')
                    cmd="SELECT * FROM students WHERE rollno="+str(rollno)
                    cursor=conn.execute(cmd)
                    profile=None
                    for row in cursor:
                        profile=row
                
                    conn.close()
                    return profile

                cam = cv2.VideoCapture(0)
                #cam =cv2.VideoCapture(2)
                cam.set(3, 1920)
                cam.set(4, 1080)
                font=cv2.FONT_HERSHEY_SIMPLEX
                while(True):
                    ret, img = cam.read()
                    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
                    faces=faceDetect.detectMultiScale(gray,1.3,5)
                    for (x,y,w,h) in faces:
                        rollno, conf = recognizer.predict(gray[y:y+h,x:x+w])  
                        cv2.rectangle(img,(x,y),(x+w,y+h),(31,0,0),2)
                        profile=getProfile(rollno)
                        if(conf<100):
                            conn=sqlite3.connect('school.db')
                            cmd="SELECT * FROM students WHERE rollno="+str(rollno)
                            cursor=conn.execute(cmd)
                            profile=None
                            for row in cursor:
                                profile=row
                            if(profile!=None):
                                emotions=(conf*100)/100
                                #print(round(emotions))
                                cv2.putText(img,str(profile[1]),(x,y+h),font,1,255,2)
                                cv2.putText(img,str(profile[2]),(x,y+h+20),font,1,255,2)
                                cv2.putText(img,str(profile[3]),(x,y+h+40),font,1,255,2)
                                #cv2.putText(img,str(profile[4]),(x,y+h+60),font,1,255,2)
                                #cv2.putText(img,str("Activity:",(x,y+h+60),font,1,255,2)
                                
                                #period change
                                now=datetime.datetime.now()
                                tt=now.strftime('%Y-%m-%d')
                                ct=now.strftime("%H:%M")
                                val="SELECT s_id from students where rollno="+str(rollno) 
                                cursor=conn.execute(val)
                                v=cursor.fetchone()
                                s_id=v[0]

                                if (emotions>35):
                                    cv2.putText(img,"Distract",(x,y+h+80),font,1,255,2)
                                    # cmd1="UPDATE students SET "+act+" = 'Distract',date='"+str(tt)+"',time='"+str(ct)+"' where rollno="+str(rollno)
                                    cmd1="INSERT INTO attendance (date,time,s_id_fk,activity) Values('"+str(tt)+"',' "+str(ct)+"',' "+str(s_id)+"','Distracted' )"
                                    cursor=conn.execute(cmd1)
                                    
                                elif(emotions<35):
                                    cv2.putText(img,"Focused",(x,y+h+80),font,1,255,2)
                                    # cmd2="UPDATE students SET "+act+" = 'Distract',date='"+str(tt)+"',time='"+str(ct)+"' where rollno="+str(rollno)
                                    cmd2="INSERT INTO attendance (date,time,s_id_fk,activity) Values('"+str(tt)+"',' "+str(ct)+"',' "+str(s_id)+"','Focused' )"
                                    cursor=conn.execute(cmd2)   
                                #elif(emotions==0):
                                #  cv2.putText(img,("Sad"),(x,y+h+60),font,1,255,2)      
                                conn.commit()
                                conn.close()    
                            
                    cv2.imshow('img',img)
                    if(cv2.waitKey(1)==ord('q')):
                        break

                cv2.destroyAllWindows()    

            win = tk.Tk()
            win.title("SCHOOL A.I")
            win.geometry("{0}x{1}+0+0".format(win.winfo_screenwidth(), win.winfo_screenheight()))
            win.wm_attributes('-type', 'splash')
            win.configure(background='light blue')
            title=Label(win,text="Live Feed",font=("times new roman",25,"bold","underline"),bg="light blue",fg="black",bd=5,relief=GROOVE)
            title.place(x=800,y=5)

            #combo box 1
            cls=["...","I","II","III","IV","V","VI","VII","VII","IX","X","XI","XII"]
            label5=Label(win,text="Select Class:",font=("times new roman",15,"bold"),bg="light blue",bd=3)
            label5.place(x=670,y=60)
            cb=ttk.Combobox(win,values=cls,width=5)
            cb.place(x=790,y=60)
            cb.current(0)

            #combo box 2
            sectn=["...","A","B","C","D"]
            label6=Label(win,text="Select Section:",font=("times new roman",15,"bold"),bg="light blue",bd=3)
            label6.place(x=870,y=60)
            cb=ttk.Combobox(win,values=sectn,width=5)
            cb.place(x=1000,y=60)
            cb.current(0)

            #submit
            Button7=Button(win, text="Submit",command=detector,font=("times new roman",12,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
            Button7.place(x=1090,y=60)

            #back
            Button8=Button(win,text="Back", command=win.destroy,font=("times new roman",20,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
            Button8.place(x=1500,y=5)

            # labelframe2=LabelFrame(win,height=800,width=1600,bd=2,relief=SUNKEN)  
            # labelframe2.place(x=160,y=100)

        def data_trainer():
            recognizer=cv2.face.LBPHFaceRecognizer_create()
            path='/home/harshit/Desktop/schoolproject/dataSet'

            def getImageWithID(path):
                imagePaths=[os.path.join(path,f) for f in os.listdir(path)]
                faces=[]
                IDs=[]
                for imagePath in imagePaths:
                    faceImg=Image.open(imagePath).convert('L')
                    faceNp=np.array(faceImg,'uint8')
                    ID=int(os.path.split(imagePath)[-1].split('.')[1])
                    faces.append(faceNp)
                    #print(ID)
                    IDs.append(ID)
                    cv2.imshow("training",faceNp)
                    cv2.waitKey(10)
                return IDs,faces

            Ids,faces=getImageWithID(path) 
            recognizer.train(faces,np.array(Ids))
            recognizer.save('/home/harshit/Desktop/schoolproject/trainner/trainingData.yml')
            cv2.destroyAllWindows()    
###################################################################################################################################
        #win=tk.Tk()       
        win.geometry("{0}x{1}+0+0".format(win.winfo_screenwidth(), win.winfo_screenheight()))
        win.wm_attributes('-type', 'splash')
        var = StringVar()
        var2=IntVar()
        var3=StringVar()
        def data_creator():
            cam = cv2.VideoCapture(0)
            faceDetect=cv2.CascadeClassifier('/home/harshit/Desktop/schoolproject/haarcascade_frontalface_default.xml')
            
            rollno=entry1.get()
            name=entry2.get()
            gender=var.get()
            class_=var2.get()
            section=var3.get()
            
            def insertOrUpdate(rollno,name,gender,class_,section):
                now=datetime.datetime.now()
                tt=now.strftime('%Y-%m-%d')
                ct=now.strftime("%H:%M")
                conn=sqlite3.connect('school.db')
                cmd="SELECT * FROM students WHERE rollno="+str(rollno)
                cursor=conn.execute(cmd)
                cmd="INSERT INTO students (rollno,name,gender,class_,section,record_date,record_time) Values('"+str(rollno)+"',' "+str(name)+"','" +str(gender)+" ','"+str(class_)+"','"+str(section)+"','"+str(tt)+"','"+str(ct)+"' )"
                
                cursor=conn.execute(cmd)
                conn.commit()
                conn.close()          
            
            insertOrUpdate(rollno,name,gender,class_,section)
            sampleNum=0
            while(True):
                ret, img = cam.read()
                gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                faces=faceDetect.detectMultiScale(gray,1.3,5)
                for (x,y,w,h) in faces:     
                    sampleNum=sampleNum+1
                    cv2.imwrite("/home/harshit/Desktop/schoolproject/dataSet/User."+str(rollno)+'.'+str(name)+'.'+str(gender)+'.'+str(sampleNum)+".jpg",gray[y:y+h,x:x+w])
                    cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
                cv2.imshow('img',img)
                cv2.waitKey(100)
                if(sampleNum>20):
                    break
            cam.release()
            cv2.destroyAllWindows() 
#############################################teacher#########################################################
        def teacher_h():
            win = tk.Tk()
            win.geometry("{0}x{1}+0+0".format(win.winfo_screenwidth(), win.winfo_screenheight()))
            win.wm_attributes('-type', 'splash')
            var1=StringVar()
            var2=StringVar()
            def teacher_live_feed():
                def teacher_detector():
                    recognizer=cv2.face.LBPHFaceRecognizer_create()
                    recognizer.read("/home/harshit/Desktop/schoolproject/tea_trainner/tea_trainingData.yml")
                    faceDetect=cv2.CascadeClassifier('/home/harshit/Desktop/schoolproject/haarcascade_frontalface_default.xml')
                    def getProfile(t_id):
                        conn=sqlite3.connect('school.db')
                        cmd="SELECT * FROM teachers WHERE t_id="+str(t_id)
                        cursor=conn.execute(cmd)
                        profile=None
                        for row in cursor:
                            profile=row               
                        conn.close()
                        return profile

                    #cam = cv2.VideoCapture(0)
                    cam =cv2.VideoCapture(2)
                    cam.set(3, 1920)
                    cam.set(4, 1080)
                    font=cv2.FONT_HERSHEY_SIMPLEX
                    while(True):
                        ret, img = cam.read()
                        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
                        faces=faceDetect.detectMultiScale(gray,1.3,5)
                        for (x,y,w,h) in faces:
                            t_id, conf = recognizer.predict(gray[y:y+h,x:x+w])  
                            cv2.rectangle(img,(x,y),(x+w,y+h),(31,0,0),2)
                            profile=getProfile(t_id)
                            if(conf<100):
                                conn=sqlite3.connect('school.db')
                                cmd="SELECT * FROM teachers WHERE t_id="+str(t_id)
                                cursor=conn.execute(cmd)
                                profile=None
                                for row in cursor:
                                    profile=row
                                if(profile!=None):
                                    emotions=(conf*100)/100
                                    #print(round(emotions))
                                    cv2.putText(img,str(profile[1]),(x,y+h),font,1,255,2)
                                    cv2.putText(img,str(profile[2]),(x,y+h+20),font,1,255,2)
                                    cv2.putText(img,str(profile[3]),(x,y+h+40),font,1,255,2)
                                    #cv2.putText(img,str(profile[4]),(x,y+h+60),font,1,255,2)
                                    #cv2.putText(img,str("Activity:",(x,y+h+60),font,1,255,2)
                                                    
                                    #period change
                                    now=datetime.datetime.now()
                                    tt=now.strftime('%Y-%m-%d')
                                    ct=now.strftime("%H:%M")
                                    val="SELECT id from teachers where t_id="+str(t_id) 
                                    cursor=conn.execute(val)
                                    v=cursor.fetchone()
                                    id=v[0]
                                    
                                    if (emotions>35):
                                        cv2.putText(img,"Distract",(x,y+h+80),font,1,255,2)
                                        # cmd1="UPDATE students SET "+act+" = 'Distract',date='"+str(tt)+"',time='"+str(ct)+"' where rollno="+str(rollno)
                                        cmd1="INSERT INTO t_attendance (date,time,t_id_fk,activity) Values('"+str(tt)+"',' "+str(ct)+"',' "+str(id)+"','Distracted' )"
                                        cursor=conn.execute(cmd1)
                                                        
                                    elif(emotions<35):
                                        cv2.putText(img,"Focused",(x,y+h+80),font,1,255,2)
                                        # cmd2="UPDATE students SET "+act+" = 'Distract',date='"+str(tt)+"',time='"+str(ct)+"' where rollno="+str(rollno)
                                        cmd2="INSERT INTO t_attendance (date,time,t_id_fk,activity) Values('"+str(tt)+"',' "+str(ct)+"',' "+str(id)+"','Focused' )"
                                        cursor=conn.execute(cmd2)   
                                                        
                                    conn.commit()
                                    conn.close()    
                                                
                        cv2.imshow('img',img)
                        if(cv2.waitKey(1)==ord('q')):
                            break
                    cv2.destroyAllWindows()  

                win = tk.Tk()
                win.title("SCHOOL A.I")
                win.geometry("{0}x{1}+0+0".format(win.winfo_screenwidth(), win.winfo_screenheight()))
                win.wm_attributes('-type', 'splash')
                win.configure(background='light blue')
                title=Label(win,text="Live Feed",font=("times new roman",25,"bold","underline"),bg="light blue",fg="black",bd=5,relief=GROOVE)
                title.place(x=800,y=5)
                #class
                label2 = Label(win, text='Class: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
                label2.place(x=670,y=60)
                list4=['1','2','3','4','5','6','7','8','9','10','11','12']
                droplist2=OptionMenu(win,var1,*list4)
                droplist2.config(width=7,font=("times new roman",15,"bold"),bg="light blue",activebackground="grey",activeforeground="light blue",relief=GROOVE)
                var1.set('....')    
                droplist2.place(x=750,y=60) 

                #section
                label3 = Label(win, text='Section: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
                label3.place(x=870,y=60)
                list5=['A','B','C','D']
                droplist3=OptionMenu(win,var2,*list5)
                droplist3.config(width=7,font=("times new roman",15,"bold"),bg="light blue",activebackground="grey",activeforeground="light blue",relief=GROOVE)
                var2.set('....')    
                droplist3.place(x=950,y=60)       
                                

                #submit
                Button7=Button(win, text="Submit",command=teacher_detector,font=("times new roman",12,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
                Button7.place(x=1090,y=60)

                #back
                Button8=Button(win,text="Back", command=win.destroy,font=("times new roman",20,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
                Button8.place(x=1500,y=5)
                win.mainloop()


            var=StringVar()
            def teacher_data_creator():
                cam = cv2.VideoCapture(2)
                faceDetect=cv2.CascadeClassifier('/home/harshit/Desktop/schoolproject/haarcascade_frontalface_default.xml')
                t_id=entry2.get()
                t_name=entry1.get()
                t_gender=var.get()
                def insertOrUpdate(t_id,t_name,t_gender):
                    now=datetime.datetime.now()
                    tt=now.strftime('%Y-%m-%d')
                    ct=now.strftime("%H:%M")
                    conn=sqlite3.connect('school.db')
                    cmd="SELECT * FROM teachers"
                    cursor=conn.execute(cmd)
                
                    cmd="INSERT INTO teachers (t_id,t_name,t_gender,record_date,record_time) Values(' "+str(t_id)+"',' "+str(t_name)+"','" +str(t_gender)+" ','"+str(tt)+"','"+str(ct)+"' )"      
                    
                    cursor=conn.execute(cmd)
                            
                    conn.commit()
                    conn.close()
                            
                insertOrUpdate(t_id,t_name,t_gender)
                sampleNum=0
                while(True):
                    ret, img = cam.read()
                    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                    faces=faceDetect.detectMultiScale(gray,1.3,5)
                    for (x,y,w,h) in faces:     
                        sampleNum=sampleNum+1
                        cv2.imwrite("/home/harshit/Desktop/schoolproject/dataSet/User."+str(t_id)+'.'+str(t_name)+'.'+str(t_gender)+'.'+str(sampleNum)+".jpg",gray[y:y+h,x:x+w])
                        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
                    cv2.imshow('img',img)
                    cv2.waitKey(100)
                    if(sampleNum>20):
                        break
                cam.release()
                cv2.destroyAllWindows() 

            def teacher_trainner():
                recognizer=cv2.face.LBPHFaceRecognizer_create()
                path='/home/harshit/Desktop/schoolproject/dataSet'

                def getImageWithID(path):
                    imagePaths=[os.path.join(path,f) for f in os.listdir(path)]
                    faces=[]
                    IDs=[]
                    for imagePath in imagePaths:
                        faceImg=Image.open(imagePath).convert('L')
                        faceNp=np.array(faceImg,'uint8')
                        ID=int(os.path.split(imagePath)[-1].split('.')[1])
                        faces.append(faceNp)
                        #print(ID)
                        IDs.append(ID)
                        cv2.imshow("training",faceNp)
                        cv2.waitKey(10)
                    return IDs,faces

                Ids,faces=getImageWithID(path) 
                recognizer.train(faces,np.array(Ids))
                recognizer.save('/home/harshit/Desktop/schoolproject/tea_trainner/tea_trainingData.yml')
                cv2.destroyAllWindows()    



            win.configure(background='light blue')
            title=Label(win,text="Teacher",font=("times new roman",25,"bold","underline"),bg="light blue",fg="black",bd=5,relief=GROOVE)
            title.place(x=850,y=5)
            #t_id
            label3=Label(win, text='Enter Teacher Id : ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
            label3.place(x=685,y=100)
            entry2 =Entry(win,width=30,bd=2)
            entry2.place(x=850,y=100)


            #name
            label1=Label(win, text='Enter Name : ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
            label1.place(x=710,y=140)
            entry1 =Entry(win,width=30,bd=2)
            entry1.place(x=850,y=140)
            #gender
            label2 = Label(win, text='Select Gender: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
            label2.place(x=710,y=180)
            list1=['Male','Female']
            droplist=OptionMenu(win,var,*list1)
            var.set('....') 
            droplist.config(width=7,font=("times new roman",15,"bold"),bg="light blue",activebackground="grey",activeforeground="light blue",relief=GROOVE)
            droplist.place(x=850,y=180)

            #take images
            Button1=Button(win,text='Take Images',command=teacher_data_creator,font=("times new roman",25,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
            Button1.place(x=850,y=300)


            #train images
            Button2=Button(win,text='Train Images',command=teacher_trainner,font=("times new roman",25,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
            Button2.place(x=850,y=400)

            #live feed
            Button3=Button(win,text='View Live Feed',command=teacher_live_feed,font=("times new roman",30,"bold"),bg="light blue",height=2,width=15,bd=6,relief=GROOVE,activebackground="grey",activeforeground="light blue")
            Button3.place(x=800,y=600)

            #back
            Button4=Button(win,text="Back", command=win.destroy,font=("times new roman",20,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
            Button4.place(x=1500,y=5)


            def reset_values():
                entry1.delete(0, 'end')
                entry2.delete(0,'end')             

            #reset all values
            Button5=Button(win, text="Reset Values",command=reset_values,font=("times new roman",15,"bold"),bg="light blue",bd=2,relief=GROOVE,activebackground="grey",activeforeground="light blue")
            Button5.place(x=1150,y=110)


            win.mainloop()
        #win.overrideredirect(True)
        win.configure(background='light blue')
        title=Label(win,text="DATA CREATER",font=("times new roman",25,"bold","underline"),bg="light blue",fg="black",bd=5,relief=GROOVE)
        title.place(x=800,y=5)
        #roll. no
        label1 =Label(win, text='Enter Roll no. : ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
        label1.place(x=710,y=60)
        entry1 =Entry(win,width=30,bd=2)
        entry1.focus()
        entry1.place(x=850,y=60)
       
        #name
        label2 =Label(win, text='Enter Name : ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
        label2.place(x=710,y=100)
        entry2 =Entry(win,width=30,bd=2)
        entry2.place(x=850,y=100)
       
        #Gender        
        #var = StringVar()
        label3 = Label(win, text='Select Gender: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
        label3.place(x=710,y=140)
        # Radiobutton(win,text="Male",variable=var,value=1,font=("times new roman",14,"bold"),bg="light blue",activebackground="light blue").place(x=850,y=140)
        # Radiobutton(win,text="Female",variable=var,value=2,font=("times new roman",14,"bold"),bg="light blue",activebackground="light blue").place(x=950,y=140)
        list1=['Male','Female']
        droplist=OptionMenu(win,var,*list1)
        var.set('....') 
        droplist.config(width=7,font=("times new roman",15,"bold"),bg="light blue",activebackground="grey",activeforeground="light blue",relief=GROOVE)
        droplist.place(x=850,y=140)

                
        #class
        label6 = Label(win, text='Class: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
        label6.place(x=710,y=180)
        list3=['1','2','3','4','5','6','7','8','9','10','11','12']
        droplist2=OptionMenu(win,var2,*list3)
        droplist2.config(width=7,font=("times new roman",15,"bold"),bg="light blue",activebackground="grey",activeforeground="light blue",relief=GROOVE)
        var2.set('....')
        droplist2.place(x=850,y=180)

        #section
        label7 = Label(win, text='Section: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
        label7.place(x=710,y=220)
        list4=['A','B','C','D']
        droplist3=OptionMenu(win,var3,*list4)
        droplist3.config(width=7,font=("times new roman",15,"bold"),bg="light blue",activebackground="grey",activeforeground="light blue",relief=GROOVE)
        var3.set('....')
        droplist3.place(x=850,y=220)

        
        #take images
        Button4=Button(win,text='Take Images',command=data_creator,font=("times new roman",25,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
        Button4.place(x=850,y=500)
        

        #train images
        Button5=Button(win,text='Train Images',command=data_trainer,font=("times new roman",25,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
        Button5.place(x=850,y=600)

        #live feed
        Button6=Button(win,text='View Live Feed',command=live_feed,font=("times new roman",30,"bold"),bg="light blue",height=3,width=20,bd=6,relief=GROOVE,activebackground="grey",activeforeground="light blue")
        Button6.place(x=1300,y=5)

        #teacher
        Button7=Button(win,text='Teacher',command=teacher_h,font=("times new roman",25,"bold"),bg="light blue",height=3,width=10,bd=6,relief=GROOVE,activebackground="grey",activeforeground="light blue")
        Button7.place(x=1400,y=200)
        #logout button
        Button6=Button(win, text="LogOut", command=win.destroy,font=("times new roman",25,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
        Button6.place(x=1450,y=875)

       

        def reset_values():
            entry1.delete(0, 'end')    
            entry2.delete(0, 'end')     

        #reset all values
        Button9=Button(win, text="Reset Values",command=reset_values,font=("times new roman",25,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
        Button9.place(x=1200,y=875)
        win.mainloop()
                    
    
root=Tk()
obj=Login_System(root)
root.mainloop()

