import os
import cv2
import numpy as np 
from PIL import Image
import sqlite3
import datetime


recognizer=cv2.face.LBPHFaceRecognizer_create()
recognizer.read("/home/harshit/Desktop/schoolproject/tea_trainner/tea_trainingData.yml")
faceDetect=cv2.CascadeClassifier('/home/harshit/Desktop/schoolproject/haarcascade_frontalface_default.xml')
def getProfile(t_id):
    conn=sqlite3.connect('school.db')
    cmd="SELECT * FROM teachers WHERE t_id="+str(t_id)
    cursor=conn.execute(cmd)
    profile=None
    for row in cursor:
        profile=row               
    conn.close()
    return profile

#cam = cv2.VideoCapture(0)
cam =cv2.VideoCapture(2)
cam.set(3, 1920)
cam.set(4, 1080)
font=cv2.FONT_HERSHEY_SIMPLEX
while(True):
    ret, img = cam.read()
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    faces=faceDetect.detectMultiScale(gray,1.3,5)
    for (x,y,w,h) in faces:
        t_id, conf = recognizer.predict(gray[y:y+h,x:x+w])  
        cv2.rectangle(img,(x,y),(x+w,y+h),(31,0,0),2)
        profile=getProfile(t_id)
        if(conf<100):
            conn=sqlite3.connect('school.db')
            cmd="SELECT * FROM teachers WHERE t_id="+str(t_id)
            cursor=conn.execute(cmd)
            profile=None
            for row in cursor:
                profile=row
            if(profile!=None):
                emotions=(conf*100)/100
                #print(round(emotions))
                cv2.putText(img,str(profile[1]),(x,y+h),font,1,255,2)
                cv2.putText(img,str(profile[2]),(x,y+h+20),font,1,255,2)
                cv2.putText(img,str(profile[3]),(x,y+h+40),font,1,255,2)
                #cv2.putText(img,str(profile[4]),(x,y+h+60),font,1,255,2)
                #cv2.putText(img,str("Activity:",(x,y+h+60),font,1,255,2)
                                
                #period change
                now=datetime.datetime.now()
                tt=now.strftime('%Y-%m-%d')
                ct=now.strftime("%H:%M")
                val="SELECT id from teachers where t_id="+str(t_id) 
                cursor=conn.execute(val)
                v=cursor.fetchone()
                id=v[0]
                
                if (emotions>35):
                    cv2.putText(img,"Distract",(x,y+h+80),font,1,255,2)
                    # cmd1="UPDATE students SET "+act+" = 'Distract',date='"+str(tt)+"',time='"+str(ct)+"' where rollno="+str(rollno)
                    cmd1="INSERT INTO t_attendance (date,time,t_id_fk,activity) Values('"+str(tt)+"',' "+str(ct)+"',' "+str(id)+"','Distracted' )"
                    cursor=conn.execute(cmd1)
                                    
                elif(emotions<35):
                    cv2.putText(img,"Focused",(x,y+h+80),font,1,255,2)
                    # cmd2="UPDATE students SET "+act+" = 'Distract',date='"+str(tt)+"',time='"+str(ct)+"' where rollno="+str(rollno)
                    cmd2="INSERT INTO t_attendance (date,time,t_id_fk,activity) Values('"+str(tt)+"',' "+str(ct)+"',' "+str(id)+"','Focused' )"
                    cursor=conn.execute(cmd2)   
                                #elif(emotions==0):
                                #  cv2.putText(img,("Sad"),(x,y+h+60),font,1,255,2)      
                conn.commit()
                conn.close()    
                            
    cv2.imshow('img',img)
    if(cv2.waitKey(1)==ord('q')):
        break

cv2.destroyAllWindows()    
                