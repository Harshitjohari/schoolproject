from tkinter import *
from PIL import ImageTk
from PIL import Image,ImageTk
from tkinter import ttk
from tkinter import messagebox
import tkinter as tk
from tkinter import Tk
import os
import cv2
import numpy as np 
from PIL import Image
import sqlite3
import datetime
from array import *
import numpy as np



win = tk.Tk()
win.geometry("{0}x{1}+0+0".format(win.winfo_screenwidth(), win.winfo_screenheight()))
win.wm_attributes('-type', 'splash')
var1=StringVar()
var2=StringVar()
def teacher_live_feed():
    def teacher_detector():
        recognizer=cv2.face.LBPHFaceRecognizer_create()
        recognizer.read("/home/harshit/Desktop/schoolproject/tea_trainner/tea_trainingData.yml")
        faceDetect=cv2.CascadeClassifier('/home/harshit/Desktop/schoolproject/haarcascade_frontalface_default.xml')
        def getProfile(t_id):
            conn=sqlite3.connect('school.db')
            cmd="SELECT * FROM teachers WHERE t_id="+str(t_id)
            cursor=conn.execute(cmd)
            profile=None
            for row in cursor:
                profile=row               
            conn.close()
            return profile

        #cam = cv2.VideoCapture(0)
        cam =cv2.VideoCapture(2)
        cam.set(3, 1920)
        cam.set(4, 1080)
        font=cv2.FONT_HERSHEY_SIMPLEX
        while(True):
            ret, img = cam.read()
            gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
            faces=faceDetect.detectMultiScale(gray,1.3,5)
            for (x,y,w,h) in faces:
                t_id, conf = recognizer.predict(gray[y:y+h,x:x+w])  
                cv2.rectangle(img,(x,y),(x+w,y+h),(31,0,0),2)
                profile=getProfile(t_id)
                if(conf<100):
                    conn=sqlite3.connect('school.db')
                    cmd="SELECT * FROM teachers WHERE t_id="+str(t_id)
                    cursor=conn.execute(cmd)
                    profile=None
                    for row in cursor:
                        profile=row
                    if(profile!=None):
                        emotions=(conf*100)/100
                        #print(round(emotions))
                        cv2.putText(img,str(profile[1]),(x,y+h),font,1,255,2)
                        cv2.putText(img,str(profile[2]),(x,y+h+20),font,1,255,2)
                        cv2.putText(img,str(profile[3]),(x,y+h+40),font,1,255,2)
                        #cv2.putText(img,str(profile[4]),(x,y+h+60),font,1,255,2)
                        #cv2.putText(img,str("Activity:",(x,y+h+60),font,1,255,2)
                                        
                        #period change
                        now=datetime.datetime.now()
                        tt=now.strftime('%Y-%m-%d')
                        ct=now.strftime("%H:%M")
                        val="SELECT id from teachers where t_id="+str(t_id) 
                        cursor=conn.execute(val)
                        v=cursor.fetchone()
                        id=v[0]
                        
                        if (emotions>35):
                            cv2.putText(img,"Distract",(x,y+h+80),font,1,255,2)
                            # cmd1="UPDATE students SET "+act+" = 'Distract',date='"+str(tt)+"',time='"+str(ct)+"' where rollno="+str(rollno)
                            cmd1="INSERT INTO t_attendance (date,time,t_id_fk,activity) Values('"+str(tt)+"',' "+str(ct)+"',' "+str(id)+"','Distracted' )"
                            cursor=conn.execute(cmd1)
                                            
                        elif(emotions<35):
                            cv2.putText(img,"Focused",(x,y+h+80),font,1,255,2)
                            # cmd2="UPDATE students SET "+act+" = 'Distract',date='"+str(tt)+"',time='"+str(ct)+"' where rollno="+str(rollno)
                            cmd2="INSERT INTO t_attendance (date,time,t_id_fk,activity) Values('"+str(tt)+"',' "+str(ct)+"',' "+str(id)+"','Focused' )"
                            cursor=conn.execute(cmd2)   
                                             
                        conn.commit()
                        conn.close()    
                                    
            cv2.imshow('img',img)
            if(cv2.waitKey(1)==ord('q')):
                break
        cv2.destroyAllWindows()  

    win = tk.Tk()
    win.title("SCHOOL A.I")
    win.geometry("{0}x{1}+0+0".format(win.winfo_screenwidth(), win.winfo_screenheight()))
    win.wm_attributes('-type', 'splash')
    win.configure(background='light blue')
    title=Label(win,text="Live Feed",font=("times new roman",25,"bold","underline"),bg="light blue",fg="black",bd=5,relief=GROOVE)
    title.place(x=800,y=5)
    #class
    label2 = Label(win, text='Class: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
    label2.place(x=670,y=60)
    list4=['1','2','3','4','5','6','7','8','9','10','11','12']
    droplist2=OptionMenu(win,var1,*list4)
    droplist2.config(width=7,font=("times new roman",15,"bold"),bg="light blue",activebackground="grey",activeforeground="light blue",relief=GROOVE)
    var1.set('....')    
    droplist2.place(x=750,y=60) 

    #section
    label3 = Label(win, text='Section: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
    label3.place(x=870,y=60)
    list5=['A','B','C','D']
    droplist3=OptionMenu(win,var2,*list5)
    droplist3.config(width=7,font=("times new roman",15,"bold"),bg="light blue",activebackground="grey",activeforeground="light blue",relief=GROOVE)
    var2.set('....')    
    droplist3.place(x=950,y=60)       
                    

    #submit
    Button7=Button(win, text="Submit",command=teacher_detector,font=("times new roman",12,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
    Button7.place(x=1090,y=60)

    #back
    Button8=Button(win,text="Back", command=win.destroy,font=("times new roman",20,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
    Button8.place(x=1500,y=5)
    win.mainloop()


var=StringVar()
def teacher_data_creator():
    cam = cv2.VideoCapture(2)
    faceDetect=cv2.CascadeClassifier('/home/harshit/Desktop/schoolproject/haarcascade_frontalface_default.xml')
    t_id=entry2.get()
    t_name=entry1.get()
    t_gender=var.get()
    def insertOrUpdate(t_id,t_name,t_gender):
        now=datetime.datetime.now()
        tt=now.strftime('%Y-%m-%d')
        ct=now.strftime("%H:%M")
        conn=sqlite3.connect('school.db')
        cmd="SELECT * FROM teachers"
        cursor=conn.execute(cmd)
    
        cmd="INSERT INTO teachers (t_id,t_name,t_gender,record_date,record_time) Values(' "+str(t_id)+"',' "+str(t_name)+"','" +str(t_gender)+" ','"+str(tt)+"','"+str(ct)+"' )"      
        
        cursor=conn.execute(cmd)
                
        conn.commit()
        conn.close()
                
    insertOrUpdate(t_id,t_name,t_gender)
    sampleNum=0
    while(True):
        ret, img = cam.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces=faceDetect.detectMultiScale(gray,1.3,5)
        for (x,y,w,h) in faces:     
            sampleNum=sampleNum+1
            cv2.imwrite("/home/harshit/Desktop/schoolproject/dataSet/User."+str(t_id)+'.'+str(t_name)+'.'+str(t_gender)+'.'+str(sampleNum)+".jpg",gray[y:y+h,x:x+w])
            cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        cv2.imshow('img',img)
        cv2.waitKey(100)
        if(sampleNum>20):
            break
    cam.release()
    cv2.destroyAllWindows() 

def teacher_trainner():
    recognizer=cv2.face.LBPHFaceRecognizer_create()
    path='/home/harshit/Desktop/schoolproject/dataSet'

    def getImageWithID(path):
        imagePaths=[os.path.join(path,f) for f in os.listdir(path)]
        faces=[]
        IDs=[]
        for imagePath in imagePaths:
            faceImg=Image.open(imagePath).convert('L')
            faceNp=np.array(faceImg,'uint8')
            ID=int(os.path.split(imagePath)[-1].split('.')[1])
            faces.append(faceNp)
            #print(ID)
            IDs.append(ID)
            cv2.imshow("training",faceNp)
            cv2.waitKey(10)
        return IDs,faces

    Ids,faces=getImageWithID(path) 
    recognizer.train(faces,np.array(Ids))
    recognizer.save('/home/harshit/Desktop/schoolproject/tea_trainner/tea_trainingData.yml')
    cv2.destroyAllWindows()    



win.configure(background='light blue')
title=Label(win,text="Teacher",font=("times new roman",25,"bold","underline"),bg="light blue",fg="black",bd=5,relief=GROOVE)
title.place(x=850,y=5)
#t_id
label3=Label(win, text='Enter Teacher Id : ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
label3.place(x=685,y=100)
entry2 =Entry(win,width=30,bd=2)
entry2.place(x=850,y=100)


#name
label1=Label(win, text='Enter Name : ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
label1.place(x=710,y=140)
entry1 =Entry(win,width=30,bd=2)
entry1.place(x=850,y=140)
#gender
label2 = Label(win, text='Select Gender: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
label2.place(x=710,y=180)
list1=['Male','Female']
droplist=OptionMenu(win,var,*list1)
var.set('....') 
droplist.config(width=7,font=("times new roman",15,"bold"),bg="light blue",activebackground="grey",activeforeground="light blue",relief=GROOVE)
droplist.place(x=850,y=180)

#take images
Button1=Button(win,text='Take Images',command=teacher_data_creator,font=("times new roman",25,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
Button1.place(x=850,y=300)


#train images
Button2=Button(win,text='Train Images',command=teacher_trainner,font=("times new roman",25,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
Button2.place(x=850,y=400)

#live feed
Button3=Button(win,text='View Live Feed',command=teacher_live_feed,font=("times new roman",30,"bold"),bg="light blue",height=2,width=15,bd=6,relief=GROOVE,activebackground="grey",activeforeground="light blue")
Button3.place(x=800,y=600)

#back
Button4=Button(win,text="Back", command=win.destroy,font=("times new roman",20,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
Button4.place(x=1500,y=5)


def reset_values():
    entry1.delete(0, 'end')
    entry2.delete(0,'end')             

#reset all values
Button5=Button(win, text="Reset Values",command=reset_values,font=("times new roman",15,"bold"),bg="light blue",bd=2,relief=GROOVE,activebackground="grey",activeforeground="light blue")
Button5.place(x=1150,y=110)


win.mainloop()
 
 
